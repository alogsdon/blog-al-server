## Running locally

Create a local MySql database in a Docker container.

```
docker run -d --name blog-al -e MYSQL_ROOT_PASSWORD=root+1 -e
MYSQL_DATABASE=blog-al -e MYSQL_USER=blog-app -e MYSQL_PASSWORD=blog+1
-p 3306:3306 -d mysql:latest
```

Or, if the container with MySql in it is already created, just start it.

```
docker start blog-al
```

Sometimes, before running the SpringBoot app, you have to log in to the MySql database with the credentials that Spring will use, or else MySql will deny Spring access.

*while the container is running:
```
docker exec -it blog-al bash
$ mysql -u blog-app -p
$ password: blog+1
```

To run the SpringBoot app, you can run com.manifestcorp.blog.BlogApplication in an IDE or run it from the command line.

```
$ ./mvnw spring-boot:run
```

After the SpringBoot app is running, you can run the front end.  Instructions for that can be found in the README.md in /blog-al-client

## Bootstrapping data in the database

In com.manifestcorp.blog.Bootstrap, you can add users/posts/comments to the MySql database on the first run of the application.  I usually have it rigged to create an admin user for myself to poke around the webapp and make sure everything is working, as there is not a way to create an admin user from the front end, only normal users.

At the moment, I have it rigged to create an admin user named Marshay with the password 'password' for you to log in with and a post so that the front end has some content to show.

## Working with the front end

To create posts, you must be logged in as an admin user or above.  To create comments, you must be logged in as a normal user or above.  To view content, you don't have to be logged in at all.

To edit posts or delete posts, you must be an admin user.  To edit comments or delete comments, you must either be the normal user who posted the comment, or an admin user. 

You can create users from the /signup page, which can be accessed from the /login page.  Two users cannot have the same username or the same email.