package com.manifestcorp.blog.payload;

import com.manifestcorp.blog.model.User;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRequest {

	private Long id;
	
	private String username;
	
	private Integer authorization;
	
	public String toString() {
		String request = "{'id': '" + this.id + "' 'username': '" + this.username + "'}";
		return request;
	}

	public static User createUserFrom(SignUpRequest request) {
		User user = new User();
		user.setUsername(request.getUsername());
		user.setEmail(request.getEmail());
		user.setPassword(request.getPassword());
		return user;
		
	}
	
	public static User createUserFrom(UserRequest request) {
		User user = new User();
		user.setId(request.getId());
		user.setUsername(request.getUsername());
		return user;
		
	}

	public static UserRequest createRequestFrom(User user) {
		UserRequest request = new UserRequest();
		request.setId(user.getId());
		request.setUsername(user.getUsername());
		request.setAuthorization(extractAuthorizationLevel(user));
		return request;
	}
	
	private static Integer extractAuthorizationLevel(User user) {
		return user.getRoles().size(); // makeshift way to check for Admin status for now
	}
}
