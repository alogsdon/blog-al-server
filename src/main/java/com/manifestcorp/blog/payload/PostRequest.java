package com.manifestcorp.blog.payload;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.manifestcorp.blog.model.Post;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PostRequest {

	private Long id;
	
	private UserRequest author;
	
	private String title;
	
	private String body;
	
	private Boolean edited;
	
	private LocalDateTime dateModified;
	
	public void setAuthor(@JsonProperty("author") UserRequest request) {
		this.author = request;
	}
	
	public String toString() {
		String request = "{'id': '" + this.id + "' 'title': '" + this.title + "' 'body': '" + this.body + "'}";
		return request;
	}
	
	public static PostRequest createRequestFrom(Post post) {
		PostRequest request = new PostRequest();
		request.setId(post.getId());
		request.setTitle(post.getTitle());
		request.setBody(post.getBody());
		request.setAuthor(UserRequest.createRequestFrom(post.getAuthor()));
		request.setEdited(post.getEdited());
		
		if (post.getEdited()) {
			request.setDateModified(post.getDateEdited());
		} else {
			request.setDateModified(post.getDateCreated());
		}
		
		return request;
	}
	
	public static Post createPostFrom(PostRequest request) {
		Post post = new Post();
		post.setId(request.getId());
		post.setTitle(request.getTitle());
		post.setBody(request.getBody());
		return post;
	}
	
}
