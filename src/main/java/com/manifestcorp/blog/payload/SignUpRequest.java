package com.manifestcorp.blog.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SignUpRequest {
	
	private String username;
	
	private String email;
	
	private String password;
	
	public String toString() {
		String request = "{'username': '" + this.username + "' email: '" + this.email + "'}";
		return request;
	}

}
