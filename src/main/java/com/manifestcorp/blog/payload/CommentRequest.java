package com.manifestcorp.blog.payload;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.manifestcorp.blog.model.Comment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommentRequest {

	private Long id;
	
	private PostRequest parent;
	
	private UserRequest author;
	
	private String body;
	
	private Boolean edited;

	private LocalDateTime dateModified;
	
	public void setParent(@JsonProperty("parent") PostRequest post) {
		this.parent = post;
	}
	
	public void setAuthor(@JsonProperty("author") UserRequest user) {
		this.author = user;
	}
	
	public String toString() {
		String request = "{'id': '" + this.id + "' 'body': '" + this.body + "'}";
		return request;
	}
	
	public static Comment createCommentFrom(CommentRequest request) {
		Comment comment = new Comment();
		comment.setId(request.getId());
		comment.setBody(request.getBody());
		
		return comment;
	}
	
	public static CommentRequest createRequestFrom(Comment comment) {
		CommentRequest request = new CommentRequest();
		request.setId(comment.getId());
		request.setBody(comment.getBody());
		request.setParent(PostRequest.createRequestFrom(comment.getParent()));
		request.setAuthor(UserRequest.createRequestFrom(comment.getAuthor()));
		request.setEdited(comment.getEdited());
		
		if (comment.getEdited()) {
			request.setDateModified(comment.getDateEdited());
		} else {
			request.setDateModified(comment.getDateCreated());
		}
		
		return request;
	}
	
}
