package com.manifestcorp.blog.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class JwtAuthResponse {

	Token token;
	
	UserRequest user;
    
    public JwtAuthResponse(String jwt, UserRequest user) {
    	this.token = new Token(jwt);
    	this.user = user;
    }
	
}
