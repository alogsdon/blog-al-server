package com.manifestcorp.blog.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginRequest {

	private String usernameOrEmail;
	
	private String password;
	
	public String toString() {
		String request = "{'identifier': '" + this.usernameOrEmail + "'}";
		return request;
	}
	
}
