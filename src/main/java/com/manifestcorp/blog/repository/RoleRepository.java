package com.manifestcorp.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manifestcorp.blog.model.Role;

@Repository("roleRepo")
public interface RoleRepository extends JpaRepository<Role, Long> {
	
}
