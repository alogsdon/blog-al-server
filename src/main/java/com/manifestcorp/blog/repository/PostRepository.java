package com.manifestcorp.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.manifestcorp.blog.model.Post;

@Repository("postRepo")
public interface PostRepository extends JpaRepository<Post, Long> {

	@Query(value = "SELECT * FROM post WHERE active = 1;",
		   nativeQuery = true)
	List<Post> findAllActive();
	
	@Query(value = "SELECT p FROM Post p WHERE LOWER(p.title) LIKE %?1% AND p.active = 1")
	List<Post> searchByTitle(String query);
	
	@Query(value = "SELECT * FROM post WHERE user_id = ?1 AND active = 1;",
		   nativeQuery = true)
	List<Post> findAllPostedBy(Long userId);
	
}