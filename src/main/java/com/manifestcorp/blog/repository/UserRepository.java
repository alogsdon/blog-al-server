package com.manifestcorp.blog.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.manifestcorp.blog.model.User;

@Repository("userRepo")
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "SELECT * FROM user WHERE LOWER(username) = LOWER(?1)",
		   nativeQuery = true)
	Optional<User> findByUsername(String username);
	
	@Query(value = "SELECT EXISTS(SELECT * FROM user WHERE LOWER(username) = LOWER(?1))",
			   nativeQuery = true)
	Integer existsByUsername(String username);

	@Query(value = "SELECT * FROM user WHERE LOWER(email) = LOWER(?1)",
		   nativeQuery = true)
	Optional<User> findByEmail(String email);

	@Query(value = "SELECT EXISTS(SELECT * FROM user WHERE LOWER(email) = LOWER(?1))",
		   nativeQuery = true)
	Integer existsByEmail(String email);

}