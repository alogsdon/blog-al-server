package com.manifestcorp.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.manifestcorp.blog.model.Comment;

@Repository("commentRepo")
public interface CommentRepository extends JpaRepository<Comment, Long> {

	@Query(value = "SELECT * FROM comment WHERE active = 1;",
		   nativeQuery = true)
	List<Comment> findAllActive();
	
	@Query(value = "SELECT * FROM comment WHERE user_id = ?1 AND active = 1;",
		   nativeQuery = true)
	List<Comment> findAllPostedBy(Long userId);
	
	@Query(value = "SELECT * FROM comment WHERE post_id = ?1 AND active = 1;",
		   nativeQuery = true)
	List<Comment> findAllRespondingTo(Long postId);
	
}
