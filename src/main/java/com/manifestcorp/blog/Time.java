package com.manifestcorp.blog;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component("time")
public class Time {
	
	public LocalDateTime now() {
		return LocalDateTime.now();
	}
	
	public LocalDateTime hoursFromNow(int hours) {
		return LocalDateTime.now().plusHours(hours);
	}
	
	public Date convertToDate(LocalDateTime ldt) {
		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}
	
}
