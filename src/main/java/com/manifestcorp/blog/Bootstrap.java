package com.manifestcorp.blog;

import com.manifestcorp.blog.model.Post;
import com.manifestcorp.blog.model.Role;
import com.manifestcorp.blog.model.RoleEnum;
import com.manifestcorp.blog.model.User;
import com.manifestcorp.blog.model.Comment;
import com.manifestcorp.blog.repository.CommentRepository;
import com.manifestcorp.blog.repository.PostRepository;
import com.manifestcorp.blog.repository.RoleRepository;
import com.manifestcorp.blog.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
class Bootstrap implements CommandLineRunner {

	@Autowired
    PostRepository postRepo;
	
	@Autowired
	CommentRepository commentRepo;

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	Encryptor encryptor;
	
	@Autowired
	Time time;

    @Override
    public void run(String... strings) {
    	
    	if (userRepo.findAll().isEmpty() && roleRepo.findAll().isEmpty()) {
    		Role user = new Role();
    		user.setRoleEnum(RoleEnum.ROLE_USER);
    		Role admin = new Role();
    		admin.setRoleEnum(RoleEnum.ROLE_ADMIN);
    		
    		roleRepo.save(admin);
    		roleRepo.save(user);
    		
    		Set<Role> roles = roleRepo.findAll().stream().collect(Collectors.toSet());
    		
    		User alex = new User();
    		alex.setEmail("alogsdon@manifestcorp.com");
    		alex.setUsername("Fran");
    		alex.setPassword(encryptor.encrypt("pass"));
    		alex.setDateCreated(time.now());
    		alex.setRoles(roles);
    		
    		userRepo.save(alex);
    	}
    }
}