package com.manifestcorp.blog.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manifestcorp.blog.Time;
import com.manifestcorp.blog.model.Comment;
import com.manifestcorp.blog.model.Post;
import com.manifestcorp.blog.model.User;
import com.manifestcorp.blog.payload.CommentRequest;
import com.manifestcorp.blog.repository.CommentRepository;
import com.manifestcorp.blog.repository.PostRepository;
import com.manifestcorp.blog.repository.UserRepository;
import com.manifestcorp.blog.error.*;

@Service
public class CommentService {

	@Autowired
	private CommentRepository commentRepo;
	
	@Autowired
	private PostRepository postRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private Time time;
	
	private final Logger log = LoggerFactory.getLogger(CommentService.class);

	public CommentRequest findById(Long id) throws CommentNotFoundException {
		Optional<Comment> maybeComment = commentRepo.findById(id);
		if (maybeComment.isPresent()) {
			return CommentRequest.createRequestFrom(maybeComment.get());
		}
		log.warn("Comment not found");
		throw new CommentNotFoundException("Comment not found");
	}

	public List<CommentRequest> findAllActive() {
		return commentRepo.findAllActive().stream().map(comment ->
			CommentRequest.createRequestFrom(comment)
		).collect(Collectors.toList());
		
	}
	
	public List<CommentRequest> findCommentsOfPost(Long id) {
		return commentRepo.findAllRespondingTo(id).stream().map(comment ->
			CommentRequest.createRequestFrom(comment)
		).collect(Collectors.toList());
	}
	
	public List<CommentRequest> findCommentsByUser(Long id) {
		return commentRepo.findAllPostedBy(id).stream().map(comment ->
			CommentRequest.createRequestFrom(comment)
		).collect(Collectors.toList());
	}

	public CommentRequest save(@Valid CommentRequest request) throws EntityNotFoundException {
		log.info("Request to create comment: {}", request);
		Comment comment = CommentRequest.createCommentFrom(request);
		comment.setDateCreated(time.now());
		Optional<Post> existingParent = postRepo.findById(request.getParent().getId());
		if (existingParent.isPresent()) {
			Post updatedParent = existingParent.get();
			updatedParent.addComment(comment);
			comment.setParent(updatedParent);
		} else {
			log.warn("Comment parent not found");
			throw new PostNotFoundException("Comment parent not found");
		}
		Optional<User> existingAuthor = userRepo.findById(request.getAuthor().getId());
		if (existingAuthor.isPresent()) {
			User updatedAuthor = existingAuthor.get();
			updatedAuthor.addComment(comment);
			comment.setAuthor(updatedAuthor);
		} else {
			log.warn("Comment author not found");
			throw new UserNotFoundException("Comment author not found");
		}
		return CommentRequest.createRequestFrom(commentRepo.save(comment));
	}

	public void deleteById(Long id) throws CommentNotFoundException {
		log.info("Request to delete comment with id: {}", id);
		Optional<Comment> maybeComment = commentRepo.findById(id);
		if (maybeComment.isPresent()) {
			Comment comment = maybeComment.get();
			comment.deactivate();
			log.info(comment.getActive().toString());
			commentRepo.save(comment);
		} else {
			log.warn("Comment not found");
			throw new CommentNotFoundException("Comment not found");
		}
	}
	
	public CommentRequest update(@Valid CommentRequest request) throws CommentNotFoundException {
		log.info("Request to update comment: {}", request);
		Optional<Comment> maybeComment = commentRepo.findById(request.getId());
		if (maybeComment.isPresent()) {
			Comment comment = maybeComment.get();
			if (request.getAuthor().getId() == comment.getAuthor().getId()
					|| request.getAuthor().getAuthorization() == 2) {
				comment.setEdited(true);
				comment.setDateEdited(time.now());
				comment.setBody(request.getBody());
				return CommentRequest.createRequestFrom(commentRepo.save(comment));
			}
			log.warn("User not authorizaed to edit this comment");
			return request;
		}
		log.warn("Comment targeted to update not found");
		throw new CommentNotFoundException("Comment targeted to update not found");
	}	
}