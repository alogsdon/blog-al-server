package com.manifestcorp.blog.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manifestcorp.blog.Time;
import com.manifestcorp.blog.error.PostNotFoundException;
import com.manifestcorp.blog.error.UserNotFoundException;
import com.manifestcorp.blog.model.Post;
import com.manifestcorp.blog.model.User;
import com.manifestcorp.blog.payload.PostRequest;
import com.manifestcorp.blog.repository.PostRepository;
import com.manifestcorp.blog.repository.UserRepository;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private Time time;
	
	private final Logger log = LoggerFactory.getLogger(PostService.class);

	public List<PostRequest> findAllActive() {
		return postRepo.findAllActive().stream().map(post ->
			PostRequest.createRequestFrom(post)
		).collect(Collectors.toList());
	}
	
	public List<PostRequest> searchByTitle(String query) {
		log.info("Request to search for post with title like: {}", query);
		return postRepo.searchByTitle(query.toLowerCase()).stream().map(post ->
			PostRequest.createRequestFrom(post)
		).collect(Collectors.toList());
	}

	public PostRequest findById(Long id) {
		Optional<Post> maybePost = postRepo.findById(id);
		if (maybePost.isPresent()) {
			return PostRequest.createRequestFrom(maybePost.get());
		}
		log.warn("Post not found");
		throw new PostNotFoundException("Post not found");
	}
	
	public List<PostRequest> findPostsByUser(Long id) {
		return postRepo.findAllPostedBy(id).stream().map(post ->
			PostRequest.createRequestFrom(post)
		).collect(Collectors.toList());
	}

	public PostRequest save(@Valid PostRequest request) throws UserNotFoundException {
		log.info("Request to create post: {}", request);
		Post post = PostRequest.createPostFrom(request);
		post.setDateCreated(time.now());
		Optional<User> existingAuthor = userRepo.findById(request.getAuthor().getId());
		if (existingAuthor.isPresent()) {
			User updatedAuthor = existingAuthor.get();
			updatedAuthor.addPost(post);
			post.setAuthor(updatedAuthor);
		} else {
			log.warn("Post author not found");
			throw new UserNotFoundException("Post author not found");
		}
		return PostRequest.createRequestFrom(postRepo.save(post));
	}

	public void deleteById(Long id) throws PostNotFoundException {
		log.info("Request to delete post with id: {}", id);
		Optional<Post> maybePost = postRepo.findById(id);
		if (maybePost.isPresent()) {
			Post post = maybePost.get();
			post.deactivate();
			postRepo.save(post);
		} else {
			log.warn("Post not found");
			throw new PostNotFoundException("Post not found");
		}
	}
	
	public PostRequest update(@Valid PostRequest request) throws PostNotFoundException {
		log.info("Request to update post: {}", request);
		Optional<Post> maybePost = postRepo.findById(request.getId());
		if (maybePost.isPresent()) {
			Post post = maybePost.get();
			if (request.getAuthor().getId() == post.getAuthor().getId()) {
				post.setEdited(true);
				post.setDateEdited(time.now());
				post.setTitle(request.getTitle());
				post.setBody(request.getBody());
				return PostRequest.createRequestFrom(postRepo.save(post));
			}
			log.warn("User not authorizaed to edit this comment");
			return request;
		}
		log.warn("Post targeted to update not found");
		throw new PostNotFoundException("Post targeted to update not found");
	}
}