package com.manifestcorp.blog.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.manifestcorp.blog.Encryptor;
import com.manifestcorp.blog.Time;
import com.manifestcorp.blog.error.UserNotFoundException;
import com.manifestcorp.blog.model.Comment;
import com.manifestcorp.blog.model.Post;
import com.manifestcorp.blog.model.User;
import com.manifestcorp.blog.payload.SignUpRequest;
import com.manifestcorp.blog.payload.UserRequest;
import com.manifestcorp.blog.repository.CommentRepository;
import com.manifestcorp.blog.repository.PostRepository;
import com.manifestcorp.blog.repository.RoleRepository;
import com.manifestcorp.blog.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	PostRepository postRepo;
	
	@Autowired
	CommentRepository commentRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	Encryptor encryptor;
	
	@Autowired
	Time time;
	
	private final Logger log = LoggerFactory.getLogger(UserService.class);
	
	public List<UserRequest> findAll() {
		return userRepo.findAll().stream().map(user ->
			UserRequest.createRequestFrom(user)
		).collect(Collectors.toList());
	}

	public UserRequest findById(Long id) throws UserNotFoundException {
		Optional<User> maybeUser = userRepo.findById(id);
		if (maybeUser.isPresent()) {
			return UserRequest.createRequestFrom(maybeUser.get());
		}
		log.warn("User not found");
		throw new UserNotFoundException("User not found");
	}
	
	public UserRequest findAuthorOfComment(Long id) throws UserNotFoundException {
		Optional<Comment> maybeComment = commentRepo.findById(id);
		if (maybeComment.isPresent()) {
			return UserRequest.createRequestFrom(maybeComment.get().getAuthor());
		}
		log.warn("Author of comment not found");
		throw new UserNotFoundException("Author of comment not found");
	}
	
	public UserRequest findAuthorOfPost(Long id) throws UserNotFoundException {
		Optional<Post> maybePost = postRepo.findById(id);
		if (maybePost.isPresent()) {
			return UserRequest.createRequestFrom(maybePost.get().getAuthor());
		}
		log.warn("Author of post not found");
		throw new UserNotFoundException("Author of post not found");
	}

	public UserRequest save(@Valid SignUpRequest request) {
		request.setPassword(encryptor.encrypt(request.getPassword()));
		log.info("Request to create user: {}", request);
		User user = UserRequest.createUserFrom(request);
		user.setDateCreated(time.now());
		user.setRoles(roleRepo.findById(Long.valueOf(1)).stream().map(role -> 
			role).collect(Collectors.toSet())); 
		return UserRequest.createRequestFrom(userRepo.save(user));
	}

	public void deleteById(Long id) throws UserNotFoundException {
		log.info("Request to delete user with id: {}", id);
		Optional<User> maybeUser = userRepo.findById(id);
		if (maybeUser.isPresent()) {
			User user = maybeUser.get();
			user.deactivate();
			userRepo.save(user);
		}
		log.warn("User not found");
		throw new UserNotFoundException("User not found");
	}
	
	public Boolean existsByEmail(String email) {
		return userRepo.existsByEmail(email) > 0;
	}
	
	public Boolean existsByUsername(String username) {
		return userRepo.existsByUsername(username) > 0;
	}

	public UserRequest update(@Valid UserRequest request) throws UserNotFoundException {
		Optional<User> maybeUser = userRepo.findById(request.getId());
		if (maybeUser.isPresent()) {
			User user = maybeUser.get();
			user.setEdited(true);
			user.setDateEdited(time.now());
			user.setUsername(request.getUsername());
			return UserRequest.createRequestFrom(userRepo.save(user));
		}
		log.warn("User targeted to update not found");
		throw new UserNotFoundException("User targeted to update not found");
	}
	
	//All below necessary to implement UserDetailsService--for Spring Security
	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		log.info("Request to log in to user: {}", usernameOrEmail);
		return findUser(usernameOrEmail);
	}
	
	public User findUser(String usernameOrEmail) throws UsernameNotFoundException {
		User persistedUser;
		try {
			persistedUser = privateFindByUsername(usernameOrEmail);
			return persistedUser;
		} catch (UsernameNotFoundException e1) {
			try {
				persistedUser = privateFindByEmail(usernameOrEmail);
				return persistedUser;
			} catch (UsernameNotFoundException e2) {
				log.warn("Username or emil {} not found", usernameOrEmail);
				throw new UsernameNotFoundException(usernameOrEmail);
			}
		}
	}
	
	private User privateFindByUsername(String username) throws UsernameNotFoundException {
		Optional<User> maybeUser = userRepo.findByUsername(username);
		if (maybeUser.isPresent()) {
			return maybeUser.get();
		}
		throw new UsernameNotFoundException(username);
	}
	
	private User privateFindByEmail(String email) throws UsernameNotFoundException {
		Optional<User> maybeUser = userRepo.findByEmail(email);
		if (maybeUser.isPresent()) {
			return maybeUser.get();
		}
		throw new UsernameNotFoundException(email);
	}
}