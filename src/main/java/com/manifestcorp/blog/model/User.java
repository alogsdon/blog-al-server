package com.manifestcorp.blog.model;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.manifestcorp.blog.Encryptor;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User extends BaseEntity implements UserDetails {
	
	//necessary to implement UserDetails--for Spring Security
	private static final long serialVersionUID = 5690606509470316972L;
	
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE) 
	@Transient
	@Autowired
	private Encryptor encryptor;
	
	@Column(name = "username")
	String username;
	@Column(name = "email")
	String email;
	@Column(name = "password")
	String password;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="author", orphanRemoval = true, cascade = CascadeType.ALL)
	@JsonManagedReference(value = "userToComments")
	List<Comment> comments;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="author", orphanRemoval = true, cascade = CascadeType.ALL)
	@JsonManagedReference(value = "userToPosts")
	List<Post> posts;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	Set<Role> roles;
	
	public void addComment(Comment comment) {
		comments.add(comment);
	}
	
	public void addPost(Post post) {
		posts.add(post);
	}
	
	public void addRole(Role role) {
		roles.add(role);
	}
	
	public void setPassword(String encrypted) {
		this.password = encrypted;
	}
	
	public String toString() {
		String user = "{'id': '" + this.id + "' 'email': '" + this.email;
		user += "' 'username': " + this.username + "'}";
		return user;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (this.getClass() != o.getClass()) return false;
		User user = (User) o;
		return this.username.equals(user.username)
				&& this.email.equals(user.email);
	}

	//necessary to implement UserDetails--for security
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles.stream().map(role ->
        			new SimpleGrantedAuthority(role.getRoleEnum().name())
			   ).collect(Collectors.toList());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}
	
}