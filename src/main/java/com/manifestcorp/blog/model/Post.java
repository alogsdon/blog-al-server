package com.manifestcorp.blog.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "post")
public class Post extends BaseEntity {
	
	@Column(name = "title")
	String title;
	@Lob
	@Column(name = "body")
	String body;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	@JsonBackReference(value = "userToPosts")
	User author;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="parent", orphanRemoval = true, cascade = CascadeType.ALL)
	@JsonManagedReference(value = "postToComments")
	List<Comment> comments;
	
	public void setAuthor(@JsonProperty("author") User user) {
		this.author = user;
	}
	
	public void addComment(Comment comment) {
		comments.add(comment);
	}
	
	public String toString() {
		String post = "{'id': '" + this.id + "' 'title': '" + this.title;
		post += "' 'body': '" + this.body + "'}";
		return post;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (this.getClass() != o.getClass()) return false;
		Post post = (Post) o;
		return this.id == post.id && this.title.equals(post.title)
				&& this.body.equals(post.body) &&
				this.dateCreated.equals(post.dateCreated) &&
				this.author.equals(post.author);
	}
}