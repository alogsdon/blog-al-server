package com.manifestcorp.blog.model;

public enum RoleEnum {
	ROLE_USER,
    ROLE_ADMIN
}
