package com.manifestcorp.blog.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(name = "created_on")
	LocalDateTime dateCreated;
	@Column(name = "edited_on")
	LocalDateTime dateEdited;
	
	@Column(name = "active")
	Boolean active = true;
	@Column(name = "edited")
	Boolean edited = false;
	
	public void deactivate() {
		this.active = false;
	}
	
}
