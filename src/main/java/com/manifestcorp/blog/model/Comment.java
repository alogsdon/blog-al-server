package com.manifestcorp.blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "comment")
public class Comment extends BaseEntity {
	
	@Lob
	@Column(name = "body")
	String body;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id")
	@JsonBackReference(value = "postToComments")
	Post parent;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	@JsonBackReference(value = "userToComments")
	User author;
	
	public void setAuthor(@JsonProperty("author") User user) {
		this.author = user;
	}
	
	public void setParent(@JsonProperty("parent") Post post) {
		this.parent = post;
	}
	
	public String toString() {
		String comment = "{'id': '" + this.id + "' 'body': '" + this.body + "'}";
		return comment;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (this.getClass() != o.getClass()) return false;
		Comment comment = (Comment) o;
		return this.id == comment.id && this.body.equals(comment.body)
				&& this.dateCreated.equals(comment.dateCreated) &&
				this.parent.equals(comment.parent) &&
				this.author.equals(comment.author);
	}

}