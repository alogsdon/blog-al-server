package com.manifestcorp.blog.controller;

import java.net.URI;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manifestcorp.blog.payload.ApiResponse;
import com.manifestcorp.blog.payload.JwtAuthResponse;
import com.manifestcorp.blog.payload.LoginRequest;
import com.manifestcorp.blog.payload.SignUpRequest;
import com.manifestcorp.blog.payload.UserRequest;
import com.manifestcorp.blog.security.JwtTokenProvider;
import com.manifestcorp.blog.security.JwtTokenValidator;
import com.manifestcorp.blog.service.UserService;

@RestController
@RequestMapping("/auth/api")
public class AuthenticationController {

	@Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    JwtTokenProvider tokenProvider;
    
    @Autowired
    JwtTokenValidator tokenValidator;
    
    private final Logger log = LoggerFactory.getLogger(AuthenticationController.class);
    
    @GetMapping("/usernameAvailability")
	public Boolean usernameAvailability(@RequestParam String username) {
		return !userService.existsByUsername(username);
	}
	
	@GetMapping("/emailAvailability")
	public Boolean emailAvailability(@RequestParam("email") String email) {
		return !userService.existsByEmail(email);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        UserRequest user = UserRequest.createRequestFrom(userService.findUser(loginRequest.getUsernameOrEmail()));

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthResponse(jwt, user));
    }
	
	@PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest request) throws URISyntaxException {
		if (userService.existsByUsername(request.getUsername())) {
			log.error("Username is already in use");
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Username is already taken"));
        } if (userService.existsByEmail(request.getEmail())) {
        	log.error("Email address is already in use");
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Email Address already in use"));
        }
        UserRequest result = userService.save(request);
        return ResponseEntity.created(new URI("/user/api/user/" + result.getId())).body(new ApiResponse(true, "User registered successfully"));
    }
	
}
