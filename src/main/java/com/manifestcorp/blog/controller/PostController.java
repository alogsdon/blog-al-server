package com.manifestcorp.blog.controller;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import com.manifestcorp.blog.payload.ApiResponse;
import com.manifestcorp.blog.payload.PostRequest;
import com.manifestcorp.blog.service.PostService;

@RestController
@RequestMapping("/post/api")
public class PostController {
	
	@Autowired
	private PostService postService;
	
	@GetMapping("/posts")
	public Collection<PostRequest> getPosts(@RequestParam (value = "q", required = false) String q) {
		if (q == null) {
			return postService.findAllActive();
		}
		return postService.searchByTitle(q);
	}
	
	@GetMapping("/post/{id}")
	public ResponseEntity<?> getPost(@PathVariable ("id") Long id) {
		try {
			PostRequest result = postService.findById(id);
	        return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}
	
	@GetMapping("/user/{id}/posts")
	public List<PostRequest> getPostsByUser(@PathVariable ("id") Long id) {
		return postService.findPostsByUser(id);
	}
	
	@PostMapping("/post")
	@Secured("ROLE_ADMIN")
    ResponseEntity<?> createPost(@Valid @RequestBody PostRequest request) {
		try {	
			PostRequest result = postService.save(request);
	        return ResponseEntity.created(new URI("/post/api/post/" + result.getId())).body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
    }

    @PutMapping("/post")
    @Secured("ROLE_ADMIN")
    ResponseEntity<?> updatePost(@Valid @RequestBody PostRequest request) {
    	try {
	        PostRequest result = postService.update(request);
	        return ResponseEntity.ok().body(result);
    	} catch (Exception e) {
    		return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
    	}
    }

    @DeleteMapping("/post/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> deletePost(@PathVariable Long id) {
    	try {
	        postService.deleteById(id);
	        return ResponseEntity.ok().body(new ApiResponse(true, "Successfully deleted"));
    	} catch (Exception e) {
    		return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
    	}
    }
}