package com.manifestcorp.blog.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import com.manifestcorp.blog.payload.ApiResponse;
import com.manifestcorp.blog.payload.UserRequest;
import com.manifestcorp.blog.service.UserService;

@RestController
@RequestMapping("/user/api")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	@Secured("ROLE_ADMIN")
	public Collection<UserRequest> getUsers() {
		return userService.findAll();
	}
	
	@GetMapping("/user/{id}")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> getUser(@PathVariable ("id") Long id) {
		try {
			UserRequest result = userService.findById(id);
	        return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}
	
	@GetMapping("/post/{id}/author")
	public ResponseEntity<?> getAuthorOfPost(@PathVariable ("id") Long id) {
		try {
			UserRequest result = userService.findAuthorOfPost(id);
			return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}
	
	@GetMapping("/comment/{id}/author")
	public ResponseEntity<?> getAuthorOfComment(@PathVariable ("id") Long id) {
		try {
			UserRequest result = userService.findAuthorOfComment(id);
			return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}
}