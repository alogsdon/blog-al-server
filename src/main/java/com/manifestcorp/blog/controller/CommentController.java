package com.manifestcorp.blog.controller;

import java.net.URI;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import com.manifestcorp.blog.payload.ApiResponse;
import com.manifestcorp.blog.payload.CommentRequest;
import com.manifestcorp.blog.service.CommentService;

@RestController
@RequestMapping("/comment/api") // TODO add @CurrentUser User currentUser as a parameter to relevant methods to make sure users can only edit and delete their own posts
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@GetMapping("/user/{id}/comments")
	public Collection<CommentRequest> getCommentsByUser(@PathVariable ("id") Long id) {
		return commentService.findCommentsByUser(id);
	}
	
	@GetMapping("/post/{id}/comments")
	public Collection<CommentRequest> getCommentsOfPost(@PathVariable ("id") Long id) {
		return commentService.findCommentsOfPost(id);
	}
	
	@PostMapping("/comment")
	@Secured("ROLE_USER")
    ResponseEntity<?> createComment(@Valid @RequestBody CommentRequest request) {
		try {	
			CommentRequest result = commentService.save(request);
	        return ResponseEntity.created(new URI("/api/comment/" + result.getId())).body(result);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}

    @PutMapping("/comment")
    @Secured("ROLE_USER")
    ResponseEntity<?> updateComment(@Valid @RequestBody CommentRequest request) {
	    try { 
    		CommentRequest result = commentService.update(request);
	        return ResponseEntity.ok().body(result);
	    } catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
    }

	@DeleteMapping("/comment/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<?> deletecomment(@PathVariable Long id) {
	    try {   
	    	commentService.deleteById(id);
	        return ResponseEntity.ok().body(new ApiResponse(true, "Successfully deleted"));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new ApiResponse(false, e.getMessage()));
		}
	}	
}