package com.manifestcorp.blog.security;

import java.time.LocalDateTime;

import io.jsonwebtoken.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.manifestcorp.blog.Time;
import com.manifestcorp.blog.model.User;

@Component
public class JwtTokenProvider {

	private final Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);

    private final String jwtSecret = "Frantjc";
    
    private final int jwtExpirationInHours = 168;
    
    @Autowired
    private Time time;

    public String generateToken(Authentication authentication) {

        User user = (User) authentication.getPrincipal();

        LocalDateTime now = time.now();
        LocalDateTime expiryDate = time.hoursFromNow(jwtExpirationInHours);
        
        return Jwts.builder()
                   .setSubject(Long.toString(user.getId()))
                   .setIssuedAt(time.convertToDate(now))
                   .setExpiration(time.convertToDate(expiryDate))
                   .signWith(SignatureAlgorithm.HS512, jwtSecret)
                   .compact();
    }
    
}
