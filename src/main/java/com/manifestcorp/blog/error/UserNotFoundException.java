package com.manifestcorp.blog.error;

public class UserNotFoundException extends EntityNotFoundException {

	public UserNotFoundException(String msg) {
		super(msg);
	}
	
	public UserNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
