package com.manifestcorp.blog.error;

public class CommentNotFoundException extends EntityNotFoundException {

	public CommentNotFoundException(String msg) {
		super(msg);
	}
	
	public CommentNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
