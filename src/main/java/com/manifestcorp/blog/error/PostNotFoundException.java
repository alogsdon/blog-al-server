package com.manifestcorp.blog.error;

public class PostNotFoundException extends EntityNotFoundException {

	public PostNotFoundException(String msg) {
		super(msg);
	}
	
	public PostNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
