package com.manifestcorp.blog;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("encryptor")
public class Encryptor {
	
	public static final PasswordEncoder ENCRYPTOR = new BCryptPasswordEncoder();
	
	public String encrypt(String sensitive) {
		if (null == sensitive) {
			return sensitive;
		}
		return ENCRYPTOR.encode(sensitive);
	}
	
}
